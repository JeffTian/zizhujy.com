﻿///<reference path="/../ZiZhuJY.Web.UI/Scripts/zizhujy.com.js"/>
///<reference path="/../ZiZhuJY.Web.UI/Scripts/Maths/zizhujy.com.MathExtended.js"/>
///<reference path="/../ZiZhuJY.Web.UI/Scripts/Latex/zizhujy.com.LaTexLex.1.2.js"/>
///<reference path="/lib/jasmine/jasmine.js"/>

describe('LaTex Lex feature', function () {
    it("Spec: run a deriv expression", function () {
        var res = zizhujy.com.LaTexLex.run('\\frac{d(2x)}{dx}');

        expect(res instanceof Array);
        expect(res.length).toBe(11);
        expect(res[0].lexeme).toBe('\\frac');
    });
});
