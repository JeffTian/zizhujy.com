﻿///<reference path="/../ZiZhuJY.Web.UI/Scripts/zizhujy.com.js"/>
///<reference path="/../ZiZhuJY.Web.UI/Scripts/mathjs/math.js"/>
///<reference path="/../ZiZhuJY.Web.UI/Scripts/Maths/zizhujy.com.MathExtended.js"/>
///<reference path="/../ZiZhuJY.Web.UI/Scripts/LaTex/zizhujy.com.LaTexLex.1.2.js"/>
///<reference path="/../ZiZhuJY.Web.UI/Scripts/LaTex/zizhujy.com.LaTexParser.1.2.js"/>
///<reference path="/../ZiZhuJY.Web.UI/Scripts/Maths/geekCalculator.js"/>
///<reference path="/lib/jasmine/jasmine.js"/>

describe('geek calculator feature', function () {
    it("Spec: parse", function () {
        expect(geekCalc.parse('\\sqrt{25}')).toBe('sqrt(25)');
        expect(geekCalc.parse('x=3')).toBe('x=3');
        expect(geekCalc.parse('x^2+y^2')).toBe('pow(x, 2) + pow(y, 2)');
        expect(geekCalc.parse('1.01^{365}')).toBe('pow(1.01, 365)');
        expect(geekCalc.parse('0.99^{365}')).toBe('pow(0.99, 365)');
        expect(geekCalc.parse('0.66`6`')).toBe('\\frac{2}{3}');
        expect(geekCalc.parse('0.142857`142857`')).toBe('\\frac{1}{7}');
    });

    it('Spec: Error thrown', function () {
        expect(function () { geekCalc.parse('x=='); }).toThrow('x==');

        expect(
            function () {
                try {
                    geekCalc.parse('x==');

                    return false;
                } catch (ex) {
                    return typeof ex === 'string' && ex === 'x==';
                }
            }()).toBe(true);
    });

    it("Spec: evaluation", function () {
        expect(geekCalc.eval('6*0.2')).toBe('=1.2=\\frac{6}{5}');
        expect(geekCalc.eval('sqrt(25)')).toBe('5');
        expect(geekCalc.eval('x=3')).toBe('3');
        expect(geekCalc.eval('y=66')).toBe('66');
        expect(geekCalc.eval('pow(1.01, 365)')).toBe('=37.78343433288715887761660479649760546027113549159100200330393389=\\frac{330911678172452160}{8758115402030107}');
        expect(geekCalc.eval('pow(0.99, 365)')).toBe('=0.02551796445229121002875033458996099289950449638640021028123795659=\\frac{388002217878591}{15205061461857824}');
        expect(geekCalc.eval('\\frac{2}{3}')).toBe('=\\frac{2}{3}=0.6666666666666666666666666666666666666666666666666666666666666667');
        expect(geekCalc.eval('\\frac{1}{7}')).toBe('=\\frac{1}{7}=0.1428571428571428571428571428571428571428571428571428571428571429');
    });

    it("Spec: parse and evaluation", function () {
        expect(geekCalc.parseAndEval('\\sqrt{25}')).toBe('5');
        expect(geekCalc.parseAndEval('0.6')).toBe('=0.6=\\frac{3}{5}');
        expect(geekCalc.parseAndEval('0.66')).toBe('=0.66=\\frac{33}{50}');
        expect(geekCalc.parseAndEval('0.66`6`')).toBe('=\\frac{2}{3}=0.6666666666666666666666666666666666666666666666666666666666666667');
    });

    it("Spec: evaluation with scope", function () {
        var scope = {};
        geekCalc.eval('x = 3', scope);

        expect(scope.x.toString()).toBe('3');
        expect(geekCalc.eval('x^2', scope)).toBe('9');

        expect(function () {
            var scope = {};
            geekCalc.parseAndEval('x = 3', scope);
            return geekCalc.parseAndEval('x^2', scope);
        }()).toBe('9');
    });
});

describe("Spec: handle pow()", function () {
    it("Spec: transform pow(x, 2) to x ** 2", () => {
        const res = geekCalc.transformPow("pow(x, 2)");
        expect(res).toEqual(['x', '**', '2'].join(' '));
    });

    it("Spec: transform 3x^2", () => {
        const res = geekCalc.transformPow("3 * pow(x, 2)");
        expect(res).toEqual(['3', '*', 'x', '**', '2'].join(' '));
    })
});

describe("Spec: deriv", function () {
    it("Spec: parse 3x^2", function () {
        var scope = {};
        var res = geekCalc.parse("3x^2", scope);
        expect(res).toBe('3 * pow(x, 2)');
    });

    it("Spec: parse x+3\\left(x+\\left(y+2\\right)\\right)", function () {
        var scope = {};
        var res = geekCalc.parse("x+3\\left(x+\\left(y+2\\right)\\right)", scope);
        expect(res).toBe('x + 3*(x + (y + 2))');
    });

    it("Spec: transform string to array", function () {
        expect(geekCalc.transformStringToArray('x + 3*(x + (y + 2))')).toEqual(['x', '+', '3', '*', ['x', '+', ['y', '+', '2']]]);
    })

    it("Spec: transform string to array for x**3", function () {
        expect(geekCalc.transformStringToArray('x ** 3')).toEqual(['x', '**', '3']);
    })

    it("Spec: parse deriv", function () {
        var scope = {};
        var res = geekCalc.parseDeriv("\\frac{d(2x)}{dx}", scope);
        expect(res).toMatch({ args: [['2', '*', 'x'], 'x'] });
        expect(res.evaluator.length).toBe(2);
        expect(res.evaluator.constructor instanceof Function).toBe(true);
    });

    it("Spec: parse deriv for $\frac{d\left(x^3\right)}{dx}$", function () {
        var scope = {};
        var res = geekCalc.parseDeriv("\\frac{d\\left(x^3\\right)}{dx}", scope);
        expect(res).toMatch({ args: [['x', '**', '3'], 'x'] });
        expect(res.evaluator.length).toBe(2);
        expect(res.evaluator.constructor instanceof Function).toBe(true);
        expect(res.args.length).toBe(2);
        expect(res.args[0]).toEqual(['x', '**', '3']);

        expect(res.evaluator(res.args[0], res.args[1])).toEqual(['3', '*', ['x', '**', '2']]);
    });

    it("Spec: parse \frac{d\left(2x\right)}{dx}", function () {
        var scope = {};
        var res = geekCalc.parseDeriv("\\frac{d\\left(2x\\right)}{dx}", scope);
        expect(res).toMatch({ args: [['2', '*', 'x'], 'x'] });
        expect(res.evaluator.length).toBe(2);
        expect(res.evaluator.constructor instanceof Function).toBe(true);
    });

    it("Spec: parse 2x", function () {
        var scope = {};
        var res = geekCalc.parseDeriv("2x", scope);
        expect(res).toBe(false);
    });

    it("Spec: parse x^2", function () {
        var res = geekCalc.parse('x^2', {});
        expect(res).toBe('pow(x, 2)');
    });

    it("Spec: parse $\frac{d\left(x^2\right)}{dx}$", function () {
        var res = geekCalc.parseDeriv("$\\frac{d\\left(x^2\\right)}{dx}$");
        expect(res).toMatch({ args: [['x', '**', '2'], 'x'] });
        expect(res.args.length).toBe(2);
        expect(res.args[0].length).toBe(3);
        expect(res.args[0][0]).toBe('x');
        expect(res.args[0][1]).toBe('**');
        expect(res.args[0][2]).toBe('2');
    });

    it("Spec: parse and evaluation deriv", function () {
        var scope = {};
        var res = geekCalc.parseAndEval('\\frac{d(2x)}{dx}', scope);
        expect(res).toBe('2');
    });

    it("Spec: beautify output", function () {
        expect(geekCalc.beautify(['3', '*', ['x', '**', '2']])).toEqual('3 \\cdot x ^ 2');
    });

    it("Spec: parse and evaluation deriv for x^2", function () {
        var res = geekCalc.parseAndEval('\\frac{d(x^2)}{dx}', {});
        expect(res).toBe('2 \\cdot x');
    });

    it("Spec: parse and evaluation deriv for $\frac{d\left(x^3\right)}{dx}$", function () {
        expect(geekCalc.parseAndEval('\\frac{d\\left(x^3\\right)}{dx}')).toBe('3 \\cdot x ^ 2');
    });

    it("Spec: parse x+y", function () {
        expect(geekCalc.parse('x+y')).toEqual('x + y');
    });

    it("Spec: parse deriv for $\frac{d\left(x+y\right)}{dx}$", function () {
        expect(geekCalc.parseDeriv('\\frac{d\\left(x+y\\right)}{dx}').args).toEqual([['x', '+', 'y'], 'x']);
    });

    it("Spec: parse and evaluation deriv for $\frac{d\left(x+y\right)}{dx}$", function () {
        expect(geekCalc.parseAndEval('\\frac{d\\left(x+y\\right)}{dx}')).toBe('1');
    });

    it("Spec: parse and evaluation deriv for $\frac{d\left(x+3\left(x+\left(y+2\right)\right)\right)}{dx}$", function () {
        expect(geekCalc.parseAndEval('\\frac{d\\left(x+3\\left(x+\\left(y+2\\right)\\right)\\right)}{dx}')).toBe('4');
    });


    // To fix it need to upgrade @jeff-tian/math
    /*
    it("Spec: parse and evaluation deriv for $\frac{d\left(3x^2\right)}{dx}$", function () {
        expect(geekCalc.parseAndEval('\\frac{d\\left(3x^2\\right)}{dx}')).toBe('6 \\cdot x');
    });
    */
})
