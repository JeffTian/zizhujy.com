﻿;
(function (global, math, Fraction, parser) {
    math.config({
        number: 'bignumber',
        precision: 64
    });

    const beautify = (element) => {
        if (element === '*') {
            return '\\cdot';
        }

        if (element === '**') {
            return '^';
        }

        if (element instanceof Array) {
            return element.map(beautify).join(' ');
        }

        return element;
    };

    var geekCalc = {
        beautify: beautify,

        transformPow: function (s) {
            return s.replace(/pow\(([^,]+?),\s*([^\)]+?)\)/, function (_, base, exponent) {
                if (arguments && arguments.length === 5) {
                    return base + ' ** ' + exponent;
                }

                return _;
            });
        },

        transformStringToArray: function (s) {
            var replaced = s
                .replace(/\(/g, '[')
                .replace(/\)/g, ']')
                .replace(/\b/g, '"')
                .replace(/\s+/g, ',')
                .replace(/([\+|\-|\/])/g, '"$1"')
                .replace(/([^*])(\*)([^*])/g, '$1"$2"$3')
                .replace(/(\*\*)/g, '"$1"')
                .replace(/""/g, '", "')
                .replace(/"\[/g, '", [');

            try {
                return JSON.parse('[' + replaced + ']');
            } catch (ex) {
                console.error('error: ', ex, ', replaced from ', s, ' to ', replaced);

                return s;
            }
        },

        parseDeriv: function (s) {
            var derivRegex = /\\frac{d(?:\\left)?\((.+?)(?:\\right)?\)}{d(\w+)}/;
            var matches = derivRegex.exec(s);

            console.log('m = ', matches, s);

            if (!matches || (matches.length !== 3)) {
                return false;
            }

            if (typeof math.deriv === 'undefined') {
                var msg = 'deriv package not loaded, please make sure you have loaded @jeff-tian/math package. Or just add a script tag in your html: <script type="text/javascript" src="https://unpkg.com/@jeff-tian/math@2.0.4/lib/math.js"></script>';
                console.error(msg);
                throw new Error(msg);
            }

            var exp = this.transformStringToArray(this.transformPow(this.parse(matches[1])));
            var v = matches[2];

            return { evaluator: math.deriv, args: [exp, v] };
        },

        parse: function (s) {
            var derivRes = this.parseDeriv(s);

            if (derivRes !== false) {
                return derivRes;
            }

            var countOfEqualSign = s.split("=").length - 1;

            parser.init(s);

            if (countOfEqualSign === 1) {
                parser.run();
            } else if (countOfEqualSign === 0) {
                parser.runSpecific(function (p) {
                    p.expression(true);
                });
            } else {

            }

            if (parser.errorList.length > 0) {
                throw parser.errorList;
            }

            if (parser.tree !== null) {
                return parser.tree.eval();
            } else {
                throw s;
            }
        },

        eval: function (s, scope) {
            if (typeof s === 'object' && s.evaluator === math.deriv) {
                const evaluated = s.evaluator(s.args[0], s.args[1]);

                console.log('evaluated = ', evaluated, s);

                return evaluated instanceof Array ? evaluated.map(beautify).join(' ') : evaluated;
            }

            var fracRegex = /\\frac{(.+)}{(.+)}/;
            var answer = s;
            var matches = fracRegex.exec(answer);

            var answer2 = '';
            var finalAnswer = '';
            var evaluator = math || global;

            if (matches === null) {
                answer = evaluator.eval(s, scope).toString();

                var frac = Fraction.newFraction(answer, false);
                if (frac.valid && frac.denominator !== 1) {
                    answer2 = '\\frac{' + frac.numerator + '}{' + frac.denominator + '}';
                }
            } else {
                answer2 = evaluator.eval(matches[1] + ' / ' + matches[2]).toString();
            }

            finalAnswer = answer;

            if (answer2 !== '') {
                finalAnswer = '=' + answer + '=' + answer2;
            }

            return finalAnswer;
        },

        parseAndEval: function (s, scope) {
            return this.eval(this.parse(s), scope);
        }
    };

    // EXPORT

    if (typeof module !== 'undefined' && module.exports) {

        global['geekCalc'] = geekCalc;
        //module.exports = geekCalc;
    } else if (typeof define === 'function' && define.amd) {
        define(function () {
            return geekCalc;
        });
    } else {
        global['geekCalc'] = geekCalc;
    }
})(this, this['math'], this['Fraction'], this['zizhujy']['com']['LaTexParser']);