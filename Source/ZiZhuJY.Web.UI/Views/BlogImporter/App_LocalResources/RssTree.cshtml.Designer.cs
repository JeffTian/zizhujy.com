﻿//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.42000
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZiZhuJY.Web.UI.Views.BlogImporter.App_LocalResources {
    using System;
    
    
    /// <summary>
    ///   一个强类型的资源类，用于查找本地化的字符串等。
    /// </summary>
    // 此类是由 StronglyTypedResourceBuilder
    // 类通过类似于 ResGen 或 Visual Studio 的工具自动生成的。
    // 若要添加或移除成员，请编辑 .ResX 文件，然后重新运行 ResGen
    // (以 /str 作为命令选项)，或重新生成 VS 项目。
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class RssTree_cshtml {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal RssTree_cshtml() {
        }
        
        /// <summary>
        ///   返回此类使用的缓存的 ResourceManager 实例。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ZiZhuJY.Web.UI.Views.BlogImporter.App_LocalResources.RssTree.cshtml", typeof(RssTree_cshtml).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   重写当前线程的 CurrentUICulture 属性，对
        ///   使用此强类型资源类的所有资源查找执行重写。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   查找类似 &lt;?xml version=\&quot;1.0\&quot; encoding=\&quot;utf-8\&quot;?&gt;
        ///&lt;methodCall&gt;
        ///  &lt;methodName&gt;metaWeblog.getPost&lt;/methodName&gt;
        ///  &lt;params&gt;
        ///    &lt;param&gt;
        ///      &lt;value&gt;
        ///        &lt;string&gt;ce4882bc-048d-41da-b4bc-de20c5b26b6e&lt;/string&gt;
        ///      &lt;/value&gt;
        ///    &lt;/param&gt;
        ///    &lt;param&gt;
        ///      &lt;value&gt;
        ///        &lt;string&gt;Jeff&lt;/string&gt;
        ///      &lt;/value&gt;
        ///    &lt;/param&gt;
        ///    &lt;param&gt;
        ///      &lt;value&gt;
        ///        &lt;string&gt;1050709&lt;/string&gt;
        ///      &lt;/value&gt;
        ///    &lt;/param&gt;
        ///  &lt;/params&gt;
        ///&lt;/methodCall&gt; 的本地化字符串。
        /// </summary>
        public static string ApiDataLoad_GetPost {
            get {
                return ResourceManager.GetString("ApiDataLoad_GetPost", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;
        ///&lt;methodCall&gt;
        /// &lt;methodName&gt;metaWeblog.newPost&lt;/methodName&gt;
        /// &lt;params&gt;
        ///  &lt;param&gt;
        ///   &lt;value&gt;
        ///    &lt;string&gt;http://www.zizhujy.com/blog/&lt;/string&gt;
        ///   &lt;/value&gt;
        ///  &lt;/param&gt;
        ///  &lt;param&gt;
        ///   &lt;value&gt;
        ///    &lt;string&gt;Jeff&lt;/string&gt;
        ///   &lt;/value&gt;
        ///  &lt;/param&gt;
        ///  &lt;param&gt;
        ///   &lt;value&gt;
        ///    &lt;string&gt;1050709&lt;/string&gt;
        ///   &lt;/value&gt;
        ///  &lt;/param&gt;
        ///  &lt;param&gt;
        ///   &lt;value&gt;
        ///    &lt;struct&gt;
        ///     &lt;member&gt;
        ///      &lt;name&gt;title&lt;/name&gt;
        ///      &lt;value&gt;
        ///       &lt;string&gt;test&lt;/string&gt;
        ///      &lt;/value&gt;
        ///     &lt;/mem [字符串的其余部分被截断]&quot;; 的本地化字符串。
        /// </summary>
        public static string ApiDataLoad_NewPost {
            get {
                return ResourceManager.GetString("ApiDataLoad_NewPost", resourceCulture);
            }
        }
    }
}
