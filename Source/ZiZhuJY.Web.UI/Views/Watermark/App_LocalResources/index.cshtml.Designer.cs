﻿//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.42000
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZiZhuJY.Web.UI.Views.Watermark.App_LocalResources {
    using System;
    
    
    /// <summary>
    ///   一个强类型的资源类，用于查找本地化的字符串等。
    /// </summary>
    // 此类是由 StronglyTypedResourceBuilder
    // 类通过类似于 ResGen 或 Visual Studio 的工具自动生成的。
    // 若要添加或移除成员，请编辑 .ResX 文件，然后重新运行 ResGen
    // (以 /str 作为命令选项)，或重新生成 VS 项目。
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class index_cshtml {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal index_cshtml() {
        }
        
        /// <summary>
        ///   返回此类使用的缓存的 ResourceManager 实例。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ZiZhuJY.Web.UI.Views.Watermark.App_LocalResources.index.cshtml", typeof(index_cshtml).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   重写当前线程的 CurrentUICulture 属性，对
        ///   使用此强类型资源类的所有资源查找执行重写。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   查找类似 Advanced Settings 的本地化字符串。
        /// </summary>
        public static string AdvancedSettingsTitle {
            get {
                return ResourceManager.GetString("AdvancedSettingsTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Bottom Center 的本地化字符串。
        /// </summary>
        public static string BottomCenter {
            get {
                return ResourceManager.GetString("BottomCenter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Bottom Left 的本地化字符串。
        /// </summary>
        public static string BottomLeft {
            get {
                return ResourceManager.GetString("BottomLeft", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Bottom Right 的本地化字符串。
        /// </summary>
        public static string BottomRight {
            get {
                return ResourceManager.GetString("BottomRight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Image: 的本地化字符串。
        /// </summary>
        public static string ChooseImage {
            get {
                return ResourceManager.GetString("ChooseImage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Add copyright message to your images on the fly! 的本地化字符串。
        /// </summary>
        public static string Description {
            get {
                return ResourceManager.GetString("Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Text Watermark 的本地化字符串。
        /// </summary>
        public static string Header {
            get {
                return ResourceManager.GetString("Header", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Image Watermarker 的本地化字符串。
        /// </summary>
        public static string ImageWatermarkLinkText {
            get {
                return ResourceManager.GetString("ImageWatermarkLinkText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Add image watermark to your images 的本地化字符串。
        /// </summary>
        public static string ImageWatermarkLinkTitle {
            get {
                return ResourceManager.GetString("ImageWatermarkLinkTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Middle Center 的本地化字符串。
        /// </summary>
        public static string MiddleCenter {
            get {
                return ResourceManager.GetString("MiddleCenter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Middle Left 的本地化字符串。
        /// </summary>
        public static string MiddleLeft {
            get {
                return ResourceManager.GetString("MiddleLeft", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Middle Right 的本地化字符串。
        /// </summary>
        public static string MiddleRight {
            get {
                return ResourceManager.GetString("MiddleRight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Add text watermark to your images on the fly! 的本地化字符串。
        /// </summary>
        public static string SeoDescription {
            get {
                return ResourceManager.GetString("SeoDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Submit 的本地化字符串。
        /// </summary>
        public static string Submit {
            get {
                return ResourceManager.GetString("Submit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Text Watermark - Add copyright message to your images on the fly! 的本地化字符串。
        /// </summary>
        public static string Title {
            get {
                return ResourceManager.GetString("Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Top Center 的本地化字符串。
        /// </summary>
        public static string TopCenter {
            get {
                return ResourceManager.GetString("TopCenter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Top Left 的本地化字符串。
        /// </summary>
        public static string TopLeft {
            get {
                return ResourceManager.GetString("TopLeft", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Top Right 的本地化字符串。
        /// </summary>
        public static string TopRight {
            get {
                return ResourceManager.GetString("TopRight", resourceCulture);
            }
        }
    }
}
