﻿using System.Web.Mvc;
using ZiZhuJY.Web.UI.Attributes;

namespace ZiZhuJY.Web.UI.Controllers
{
    [Localization]
    public class GeekCalculatorController : Controller
    {
        //
        // GET: /GeekCalculator/

        [OutputCache(CacheProfile = "Cache30Days")]
        [ETag]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TestParser()
        {
            return View();
        }
    }
}
