﻿using System.Web.Mvc;
using ZiZhuJY.Web.UI.Attributes;

namespace ZiZhuJY.Web.UI.Controllers
{
    [Localization]
    public class PlotterController : Controller
    {
        //
        // GET: /Plotter/
        [OutputCache(CacheProfile="Cache30Days")]
        [ETag]
        public ActionResult Index()
        {
            return View("Plotter");
        }
    }
}
