﻿using System.Web.Mvc;
using ZiZhuJY.Web.UI.Attributes;

namespace ZiZhuJY.Web.UI.Controllers
{
    [Localization]
    public class PureController : Controller
    {
        //
        // GET: /Pure/

        [OutputCache(CacheProfile = "Cache30Days")]
        [ETag]
        public ActionResult Index()
        {
            return View();
        }

    }
}
