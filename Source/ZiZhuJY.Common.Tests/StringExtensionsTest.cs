﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZiZhuJY.Common.Extensions;

namespace ZiZhuJY.Common.Tests
{
    [TestClass]
    public class StringExtensionsTest
    {
        [TestMethod]
        public void SubStrTests()
        {
            "Test".Substr(0, 4).Should().Be("Test");
        }

        [TestMethod]
        public void FirstLetterToLowerTests()
        {
            "_TestTest".FirstLetterToLower().Should().Be("_testTest");
            "home".FirstLetterToLower().Should().Be("home");
            "Home".FirstLetterToLower().Should().Be("home");
            "hOme".FirstLetterToLower().Should().Be("hOme");
        }

        [TestMethod]
        public void FirstLetterToUpperTests()
        {
            "plotter".FirstLetterToUpper().Should().Be("Plotter");
            "_plotter".FirstLetterToUpper().Should().Be("_Plotter");
            "Home".FirstLetterToUpper().Should().Be("Home");
        }
    }
}
